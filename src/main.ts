import Developer from "./classes/Developer";
import Game from "./classes/Game";
import VideoGame from "./classes/VideoGame";
import PlatformPC from "./interfaces/PlatformPC";
import PlatformPS3 from "./interfaces/PlatformPS3";

// j'ai créé un objet de la classe Game (instance)
// const valve = new Developer("Valve", "USA")
const valve = new Developer("Valve",
  {
    street: 'lorem',
    zipCode: 'Z86468',
    city: 'Hostin',
    country: 'USA'
  }
);
const platformPC = new PlatformPC();
const platformPS3 = new PlatformPS3();
// on peut créer un objet littéral si on n'a pas de méthode dans la classe
const portal = new VideoGame("Portal", "Jeu d'énigme à la première personne", [1, 1], valve, [platformPC, platformPS3]);

console.log(portal.getInfo())
