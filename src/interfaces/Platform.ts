import VideoGame from "../classes/VideoGame"

interface Platform {

  name: string;

  getGameList(): VideoGame[];

  launchGame(game: VideoGame): void;

}

export default Platform;