import VideoGame from "../classes/VideoGame";

interface PhysicalGames {

  insertGame(game: VideoGame): void;

  ejectGame(): void;
}

export default PhysicalGames;