import VideoGame from "../classes/VideoGame";
import PhysicalGames from "./PhysicalGames";
import Platform from "./Platform";

class PlatformGamecube implements Platform, PhysicalGames {
  name: string = 'GameCube';

  getGameList(): VideoGame[] {
    throw new Error("Method not implemented.");
  }

  launchGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  insertGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  ejectGame(): void {
    throw new Error("Method not implemented.");
  }
}

export default PlatformGamecube;