import VideoGame from "../classes/VideoGame";
import DigitalGames from "./DigitalGames";
import PhysicalGames from "./PhysicalGames";
import Platform from "./Platform";

class PlatformPC implements Platform, DigitalGames, PhysicalGames {

  name: string = 'PC';

  getGameList(): VideoGame[] {
    return [];
  }

  launchGame(game: VideoGame): void {
    console.log("Le jeu " + game.name + " est lancé sur PC");
  }

  insertGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  ejectGame(): void {
    throw new Error("Method not implemented.");
  }

  downloadGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  removeGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }
}

export default PlatformPC;