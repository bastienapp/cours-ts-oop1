import VideoGame from "../classes/VideoGame";
import DigitalGames from "./DigitalGames";
import Platform from "./Platform";

class PlatformOuya implements Platform, DigitalGames {

  name: string = 'Ouya';

  getGameList(): VideoGame[] {
    throw new Error("Method not implemented.");
  }

  launchGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  downloadGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }

  removeGame(game: VideoGame): void {
    throw new Error("Method not implemented.");
  }
}