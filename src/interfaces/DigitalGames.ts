import VideoGame from "../classes/VideoGame";

interface DigitalGames {

  downloadGame(game: VideoGame): void;

  removeGame(game: VideoGame): void;
}

export default DigitalGames;