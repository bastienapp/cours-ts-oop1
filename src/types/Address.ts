export type Address = {

  street: string;
  zipCode: string;
  city: string;
  country: string;
}