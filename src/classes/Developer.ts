import { Address } from "../types/Address";

export default class Developer {

  constructor(
    public name: string,
    public address: Address
  ) {}

  getDeveloperInfo(): string {
    return 'Name: ' + this.name + ' - '
      + 'Address: ' + this.address.street + ', ' +
        this.address.zipCode + ', ' +
        this.address.city + ', ' +
        this.address.country;
  }
}