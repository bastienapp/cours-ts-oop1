import Platform from "../interfaces/Platform";
import Developer from "./Developer";
import Game from "./Game";

export default class VideoGame extends Game {

  developer: Developer
  platformList: Platform[]

  constructor(
    name: string,
    description: string,
    nbPlayers: [number, number],
    developer: Developer,
    platformList: Platform[]
  ) {
    super(name, description, nbPlayers);
    this.developer = developer;
    this.platformList = platformList
  }

  // polymorphisme
  getInfo(): string {
    let result = '';
    result += `Studio: ${this.developer.getDeveloperInfo()}\n`;
    result += 'Platform: \n';
    for (const platform of this.platformList) {
      result += '- ' + platform.name + '\n';
    }
    return super.getInfo() + result;
  }
}