/*
Classe : permet de créer des instances d'objet : on défini un moule (propriétés et méthodes) et le constructeur permet de créer des instances en hydratant les propriétés
Type : structurer des données (dans le cas où je n'ai pas besoin de méthodes), ex : Address avec street, city, zipCode
Classe abstraite : permet de créer un "moule" qui sera hérité par d'autres classes; je ne peux pas faire des instances d'une classe abstraite
Interface : créé un contrat : la classe qui implémente mon interface devra obligatoirement renseigner les propriétés et méthodes de l'interface

*/

abstract class Game {

  constructor(
    public name: string, // propriété
    public description: string,
    public nbPlayers: [number, number]
  ) {
    // pas besoin d'hydrater les proriétés
  }

  getInfo(): string {

    let result = '';
    result += `Title: ${this.name}\n`;
    result += `Description: ${this.description}\n`;
    if (this.nbPlayers[0] === this.nbPlayers[1]) {
      result += `Player\'s count: ${this.nbPlayers[0]}\n`;
    } else {
      result += `Player\'s count: from ${this.nbPlayers[0]} to ${this.nbPlayers[1]}\n`;
    }

    return result;
  }
}

export default Game;